# Chip microfluídica

Probamos con el chip de microfluídica escalera con un OFM 40X pero no logramos hacer foco: hay que aumentar la distancia focal.

<figure align="center">
    <img src="https://gitlab.com/luherbert/wiki-regosh-2023/-/raw/main/imagenes/hardware/microscopía/microfluidica/microfluidica1.jpeg" alt="Microfluídica" width="300"/> 
    <img src="https://gitlab.com/luherbert/wiki-regosh-2023/-/raw/main/imagenes/hardware/microscopía/microfluidica/microfluidica2.jpeg"  alt="Microfluídica" width="300"/>
    <figcaption style="font-size:small;"> Microfluídica </figcaption>
</figure>

Discutimos si es posible reciclar los vidrios de las placas de microscopia, que son de 0.17 mm.

El adhesivo que usan para pegar el vidrio (gernam glass?) a la placa es "USP class 4", quizás sea una silicona, y haya que sacarlo con algún solvente power y calor. 