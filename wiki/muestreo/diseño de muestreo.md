# Diseño de Muestreo

Durante la residencia, exploramos hipotéticamente cómo deberíamos estructurar un futuro plan de muestreo. Nuestro objetivo principal es obtener una amplia información sobre la presencia de patógenos ambientales como Cryptosporidium y Giardia, así como sobre sus vectores, principalmente el Castor, en distintos puntos de la región.

Guiados por Octavio, entendimos que la mejor práctica en el diseño de muestreo implica evaluar cómo diferentes técnicas se comportarían en situaciones basadas en escenarios reales. Esta evaluación se realiza utilizando el método de "bootstraping" en un modelo teórico de la región, considerando una distribución anticipada de patógenos. El proceso de diseño se dividió en dos fases:

![Diagrama de Muestreo](../muestreo/imagenes/diagrama.PNG "Diagrama del Proceso")

1. Fran diseñó un "simulador de escenarios" en Python que, utilizando información del entorno físico (como altitud y cauces fluviales), produce escenarios aleatorios de la distribución de patógenos en la región.
 
2. Octavio creó un script en R que simula el resultado de un muestreo en el mundo real. Este script, además, utiliza estos resultados para intentar deducir la posible distribución de patógenos y vectores en la región.

## Tareas Pendientes

1. Fusionar ambos proyectos para que los scripts creados por Octavio puedan ser implementados en un escenario semi-real generado por el "simulador de escenarios" de Fran.

2. Refinar el simulador de escenarios basándonos en estudios previos, incorporando parámetros como la percolación o descomposición de la carga patogénica. También sería útil incorporar datos de investigaciones ecológicas sobre castores para representar de manera más precisa su nicho ecológico.

## Simulador de Escenarios

<iframe id="mapa" src="../code/simulador_de_escenarios/combined_map.html" width="600" height="300" align="center" style="margin-bottom:50px;"> </iframe>

[El código](https://gitlab.com/FranQuero95/wiki-regosh-2023/-/tree/main/wiki/muestreo/code/simulador_de_escenarios) está diseñado para simular un modelo de contaminación por patógenos, en este caso relacionado con Giardia o Cryptosporidium. Estos patógenos son introducidos por una especie invasora novedosa que actúa como hospedador, específicamente el castor (Castor), en una cuenca fluvial determinada, a saber, Bahía Exploradores, Aysén, Chile. El código carga datos geográficos e hidrológicos de la región, simula N puntos de infección (junto con un radio de infectividad) que podrían contaminar un camino fluvial. Usando capas topográficas, la información de altura determina la propagación de la contaminación, afectando todos los caminos fluviales aguas abajo del río infectado.

**Específicamente, el código:**

- Carga datos geográficos de los archivos GeoJSON proporcionados (Altura, cobertura, cuencas fluviales...).
- Genera puntos aleatorios que representan las ubicaciones de origen de la contaminación.
- La capa "Cobertura.geojson" contiene la máscara con las ubicaciones de origen permitidas (p.ej. excluyendo ríos).
- Visualiza áreas de infección potenciales basadas en estos puntos.
- Resalta áreas de intersección entre las zonas de infección y los caminos fluviales.
- Calcula los caminos fluviales "downstream" del punto de infección y los señala como contaminados.
- Exporta las visualizaciones finales como un mapa HTML interactivo.

**Uso:**

1. Asegúrate de que los archivos GeoJSON, GeoTIFFs y las fotos asociadas estén en los directorios correctos.
2. Ejecuta 'pip install -r requirements.txt'.
3. Ejecuta el código.
4. Abre el archivo generado 'combined_map.html' en un navegador web para ver el mapa interactivo.

**Tareas pendientes:**

1. Introducir la degradación/percolación al modelo de infectividad, basado en la literatura.
2. Utilizar datos ecológicos para guiar la generación de castores.

## Simulador de Muestreo
