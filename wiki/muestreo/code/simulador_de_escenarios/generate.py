"""
-------------------------------------------------------------------------------
Written by Fran Quero for reGOSH 2023 Chile. Licensed under GPL-3.0.

This code is designed to simulate a pathogen contamination model, in this case involving Giardia 
or Cryptosporidium. These pathogens are introduced by a novel invasive species that acts as a host, 
specifically the beaver (Castor), in a certain river basin, namely Bahía Exploradores, Aysen, Chile. 
The code loads geographical and hydrological data from the region, simulates N infection points (along 
with a radius of infectivity) that could contaminate a river path. Using topographical layers, height 
information determines contamination spread, affecting all fluvial paths downstream of the infected river.

Specifically, the code:
- Loads geographical data from provided GeoJSON files.
- Generates random points representing source locations of contamination. 
 - The layer "Cobertura.geojson" contains the mask with the allowed source locations (f.e. excluding rivers)
- Visualizes potential infection areas based on these points.
- Highlights areas of intersection between the infection zones and river paths.
- Incorporates height data as an additional layer to provide context.
- Exports the final visualizations as an interactive HTML map.

Use:
1. Ensure required GeoJSON files, GeoTIFFs, and associated photos are in the correct directories.
2. Run 'pip install -r requirements.txt'.
3. Execute the code.
4. Open the generated 'combined_map.html' in a web browser to view the interactive map.

To-Dos:
1. Introduce decay/percolation to the infectivity model, based on literature.
2. Use ecological data to guide the beaver generation.
-------------------------------------------------------------------------------
"""

# Standard library imports
import random
from collections import namedtuple

# Third-party library imports
import geopandas as gpd
import pandas as pd
import folium
import numpy as np
import rasterio
from rasterio.plot import show
from shapely.geometry import Point
from folium.features import CustomIcon
from geopandas.tools import sjoin
import matplotlib.pyplot as plt

# PARAMETERS
N = 20                  # number of source points
R = 2                   # radius of infection
variance_range = (-0.5, 0.5)  # variance of the infectious range
source_icon = "castor.png"

# HELPER FUNCTIONS
def generate_random_points(geo_df, cobertura, N):
    """
    Generate a list of N random points within the bounds of the given GeoDataFrame,
    but also ensuring they lie within the 'Cobertura' infill.

    Args:
    - geo_df: A GeoDataFrame within whose bounds the points should be generated.
    - cobertura: A GeoDataFrame representing the Cobertura layer.
    - N: Number of points to generate.

    Returns:
    - A GeoDataFrame containing the random points.
    """
    # Get the bounds of the geo_df for initial point generation
    minx, miny, maxx, maxy = geo_df.total_bounds

    points = []
    for _ in range(N):
        x = random.uniform(minx, maxx)
        y = random.uniform(miny, maxy)
        point = Point(x, y)
        
        # Check if point is within the Cobertura infill
        while not cobertura.contains(point).any():
            x = random.uniform(minx, maxx)
            y = random.uniform(miny, maxy)
            point = Point(x, y)
        
        points.append(point)

    return gpd.GeoDataFrame(geometry=points, crs=geo_df.crs)

def generate_infection_layer(points_gdf, R, variance_range):
    """
    Generate an infection layer based on given points, radius R, and variance range.

    Parameters:
    - points_gdf (GeoDataFrame): The GeoDataFrame containing the points.
    - R (float): Base radius (in kilometers) for the infection zone around each point.
    - variance_range (tuple): A tuple containing the min and max random variance (in kilometers) to add to the base radius.

    Returns:
    - GeoDataFrame: A GeoDataFrame containing the buffered polygons representing the infection zones.
    """
    
    # Convert R and variance_range to meters (since 1 km = 1000 meters)
    R_meters = R * 1000
    variance_range_meters = (variance_range[0] * 1000, variance_range[1] * 1000)
    
    # Project the points to a metric CRS (World Mercator)
    points_gdf_projected = points_gdf.to_crs(epsg=3395)
    
    buffered_geoms = []
    for _, row in points_gdf_projected.iterrows():
        variance = np.random.uniform(variance_range_meters[0], variance_range_meters[1])
        buffer_polygon = row['geometry'].buffer(R_meters + variance)
        buffered_geoms.append(buffer_polygon)

    # Convert buffered polygons to a GeoDataFrame
    infection_layer_projected = gpd.GeoDataFrame(geometry=buffered_geoms, crs=points_gdf_projected.crs)
    
    # Project the infection layer back to the original CRS
    infection_layer = infection_layer_projected.to_crs(points_gdf.crs)
    
    return infection_layer
    
def get_height_value(data, x, y, bounds):
    """Retrieve the height value from the numpy data array for given x, y coordinates."""
    rows, cols = data.shape
    col = int((x - bounds.left) / (bounds.right - bounds.left) * cols)
    row = int((bounds.top - y) / (bounds.top - bounds.bottom) * rows)
    return data[row, col]

def check_infections_intersection(infection_layer, lines_gdf, height_data, bounds, cycles=20):
    """
    Check if any of the infectious radii intersects with any of the lines or polygons in another GeoDataFrame.
    Also, it identifies neighboring intersections in cycles based on height constraints. 
    Return the intersecting geometries.

    Parameters:
    - infection_layer (GeoDataFrame): The GeoDataFrame containing infectious radii (buffered points).
    - lines_gdf (GeoDataFrame): The GeoDataFrame containing lines or polygons to check against.
    - height_data (numpy.array): The numpy array containing height data.
    - bounds (namedtuple): A namedtuple containing the bounds of the height data.
    - cycles (int): Number of times to search for neighboring intersections.

    Returns:
    - GeoDataFrame: A GeoDataFrame containing the intersecting geometries from lines_gdf.
    """
    
    # Use sjoin to find the initial intersection between the infectious radii and the lines
    intersections = sjoin(infection_layer, lines_gdf, how="inner", op="intersects")
    intersected_ids = set(intersections['index_right'].values)

    # Loop for identifying neighboring intersections
    for cycle in range(cycles):
        # Extract the geometries from lines_gdf that are in the intersected_ids set
        initial_intersected_geometries = lines_gdf.loc[list(intersected_ids)]

        # Perform the spatial join to find neighboring intersecting geometries
        neighbors = sjoin(initial_intersected_geometries, lines_gdf, how='inner', op='intersects')
        
        # If not the first cycle, check height values from raster
        if cycle > 0:
            new_intersected_ids = set()
            for _, row in neighbors.iterrows():
                origin_x, origin_y = row.geometry.centroid.x, row.geometry.centroid.y
                neighbor_x, neighbor_y = lines_gdf.loc[row['index_right']].geometry.centroid.x, lines_gdf.loc[row['index_right']].geometry.centroid.y
                
                origin_height = get_height_value(height_data, origin_x, origin_y, bounds)
                neighbor_height = get_height_value(height_data, neighbor_x, neighbor_y, bounds)
                
                if neighbor_height < origin_height:
                    new_intersected_ids.add(row['index_right'])
            
            # Update the intersected_ids with these newly found intersecting geometries
            intersected_ids.update(new_intersected_ids)
        else:
            # For the first cycle, simply update the intersected_ids
            intersected_ids.update(neighbors['index_right'].values)
    
    # Return only the unique intersecting geometries based on the intersected_ids
    return lines_gdf.loc[list(intersected_ids)]

def sample_icon_paste (points, map):
    # Assuming m is your folium.Map object
    icon_url = source_icon

    for idx, row in points.iterrows():
        location = [row['geometry'].y, row['geometry'].x]
        
        # Using CustomIcon to define the custom PNG icon
        icon = CustomIcon(
            icon_url,
            icon_size=(50, 50),  # Size of the image, adjust as needed
        )
        
        # Adjust the marker's position to be slightly above the actual point.
        # This can be done by adjusting the latitude.
        # The actual adjustment will depend on the zoom level and CRS, so you might need to fine-tune this.
        adjusted_location = [location[0] + 0.001, location[1]]  # Adjust the 0.001 value to move the image
        
        # Add a marker with the custom icon
        folium.Marker(
            location=adjusted_location,
            icon=icon
        ).add_to(map)
        
        # Continue with your CircleMarker as before
        folium.CircleMarker(
            location=location,
            radius=3,
            color='blue',
            fill=True,
            fill_color='blue'
        ).add_to(map)

def geotiff_to_png_and_bounds(geotiff_path, png_path):
    with rasterio.open(geotiff_path) as dataset:
        data = dataset.read(1)
        bounds = dataset.bounds

        # Scale the data values to 0-255 for PNG display
        scaled_data = (data - data.min()) / (data.max() - data.min()) * 255
        plt.imsave(png_path, scaled_data, cmap='gray')
        
        # Check the CRS and transform bounds if necessary
        if dataset.crs != "EPSG:4326":
            # Transform bounds to EPSG:4326
            transformed_bounds = rasterio.warp.transform_bounds(dataset.crs, "EPSG:4326", *bounds)
            # Creating a named tuple for bounds
            Bounds = namedtuple("Bounds", ["left", "bottom", "right", "top"])
            bounds = Bounds(transformed_bounds[0], transformed_bounds[1], transformed_bounds[2], transformed_bounds[3])

    return scaled_data, bounds

# MAIN EXECUTION
print("Loading GeoDataFrames...")

# Load GeoDataFrames
data1 = gpd.read_file('HidroLayer.geojson')
data2 = gpd.read_file('Cobertura.geojson')

print("Loading GeoTIFF...")
height_data, bounds = geotiff_to_png_and_bounds("height.tif", "height.png")

print("Ensuring CRS is WGS84...")
data1 = data1.to_crs(epsg=4326)
data2 = data2.to_crs(epsg=4326)

print("Generating random points and infection layer...")
source_points = generate_random_points(data1, data2, N)
infection_layer = generate_infection_layer(source_points, R, variance_range)

print("Initializing folium map...")
map_center = [data1.geometry.centroid.iloc[0].y, data1.geometry.centroid.iloc[0].x]
m = folium.Map(location=map_center, zoom_start=12)

print("Checking intersections and styling layers...")
intersected_gdf = check_infections_intersection(infection_layer, data1, height_data, bounds)
intersected_ids = set(intersected_gdf['Name'].values)

# STYLE FUNCTIONS
def style_function_HidroLayer(feature):
    if feature["properties"]['Name'] in intersected_ids:  # get the index from the id
        return {'fillColor': '#FF0000', 'color': '#FF0000'}  # red color for intersected shapes
    else:
        return {'fillColor': '#0000FF', 'color': '#0000FF'}  # blue color for non-intersected shapes
    
def style_function_height(feature):
    # Style based on the 'raster_val' property of your GeoDataFrame
    if feature["properties"]['raster_val'] == 1:
        return {'fillColor': '#8A2BE2', 'color': '#8A2BE2'}  # Some color, change as needed

print("Generating folium map layers...")
# Add GeoDataFrames to the map
folium.GeoJson(data1, name="HidroLayer", style_function=style_function_HidroLayer).add_to(m)
sample_icon_paste(source_points, m)
folium.GeoJson(infection_layer, name="Infection Layer", style_function=lambda x: {'fillColor': '#FF0000'}).add_to(m)

# (Uncomment if you want to add the height layer to the folium map)
# folium.raster_layers.ImageOverlay(...).add_to(m)

folium.LayerControl().add_to(m)

# Save the map to HTML
m.save('combined_map.html')

print("Folium map created!")
